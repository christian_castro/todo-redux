import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { Link, Redirect, useHistory } from "react-router-dom"
import { Button, TextField } from "@material-ui/core"
import { ContainerRegister, ImageAndForm } from "./styles"
import { toast } from "react-toastify";
import registerImage from "../../assets/Register.svg"
import * as yup from "yup"
import axios from "axios";

export const FormRegister = ({ autenticado }) => {

    const history = useHistory()

    const formSchema = yup.object().shape({
        username: yup.string(),
        password: yup.string().min(8, 'Minímo de 8 digitos'),
        email: yup.string().email('Email inválido'),
    })

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(formSchema)
    })

    const onSub = (data) => {
        axios.post("https://kenzieshop.herokuapp.com/users/", data)
        .then(res => {
            console.log(res.data)
            toast.success('Cadastro concluído')
            return history.push("/login")

        })
        .catch(_ => toast.error('Email já cadastrado'))
    }
    
    if(autenticado) {
        return <Redirect to={"/dashboard"} />
    }

    return (
        <div>
            <br />
            <ImageAndForm>
            <img src={registerImage} alt={registerImage} />

            <ContainerRegister onSubmit={handleSubmit(onSub)}>
            <h1>Cadastro</h1>

                <TextField 
                  required 
                  size="small" 
                  label="Username" 
                  variant="outlined" 
                  {...register("username")} 
                  />
                  <br /><br />

                <TextField 
                  required 
                  size="small" 
                  label="E-mail" 
                  variant="outlined"
                  {...register("email")}
                  />
                <br />
                {errors.email && errors.email.message}
                <br />

                <TextField 
                  required 
                  size="small" 
                  label="Senha" 
                  variant="outlined" 
                  type="password" 
                  {...register("password")} 
                  />
                  <br />

                  <br />

                  {errors.confirmPassword && errors.confirmPassword.message}

                <Button color="primary" variant="contained" type="submit">Cadastre-se</Button>
            <p>Já possui uma conta? <Link to={"/login"}>Entrar</Link></p>
            </ContainerRegister>
            </ImageAndForm>
        </div>
    )
}