import notFound from "../../assets/notFound.svg"
import { useHistory } from "react-router-dom"
import { Button } from "@material-ui/core"

export const PageNotFound = () => {

    const styledImg = {height: "300px"}
    const styledContainer = {paddingTop: "10%"}
    const styledTitle = {fontFamily: "font-family: 'Roboto Condensed', sans-serif", fontSize: "2.4rem", color: "#fff"}
    const styledContainerParent = {background: "#801818", minHeight: "102.5vh"}
    const styledButton = {marginTop: "2rem", width: "10rem", fontWeight: "bold"}

    const history = useHistory();
    return (
        <div style={styledContainerParent}>
            <div style={styledContainer}>
                <h1 style={styledTitle}>Não foi encontrado nada aqui :(</h1>
            </div>
            <img src={notFound} alt={notFound} style={styledImg}/>
            <div>
                <Button style={styledButton} variant="contained" color="secondary" size="large" onClick={() => history.push("/")}>INÍCIO</Button>
            </div>
        </div>
    )
}